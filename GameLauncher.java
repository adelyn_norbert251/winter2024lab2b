import java.util.Scanner;
public class GameLauncher{
	public static void main(String[] args){
	
	System.out.println("Hello, please chose a game to play ! Option 1 is Wordle and option 2 is Hangman");
	Scanner reader = new Scanner (System.in);
	int choice = reader.nextInt();
	gameChoice(choice);
	}
	public static void gameChoice(int x){ // if you chose option 1 run the game wordle if not run the game hangman
		if(x == 1){
			runGame1();
		}
		else{
			runGame2(); 
		}
	}
	public static void runGame1(){
		//Start worlde game
		wordle.runGame();
	}
	public static void runGame2(){
		Scanner reader = new Scanner (System.in);
		//Get user input
		System.out.println("Enter a 4-letter word:");
		String word = reader.next();
		word = word.toUpperCase();
		//Start hangman game
		Hangman.runGame(word);
	}
}
