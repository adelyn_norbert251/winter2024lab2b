import java.util.Scanner;
public class Hangman{

	public static int isLetterInWord(String word, char c) {
	for (int i =0; i<4 ; i++){
	
		if (word.charAt(i)==c){
			return i;
		}
			
	}
	   return -1;
}
	public static char toUpperCase(char c) {
		c = Character.toUpperCase(c);
		return c;
	}
	public static void printWork(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3) {
		String newWord = "";
		if (letter0 == true){
			newWord= newWord+ word.charAt(0);
			
		}
		else {
			newWord = newWord+" _ ";
		}
		if (letter1 == true){
			newWord= newWord+ word.charAt(1);
			
		}
		else {
			newWord = newWord+" _ ";
		}
		if (letter2 == true){
			newWord= newWord+ word.charAt(2);
			
		}
		else {
			newWord = newWord+" _ ";
		}

		if (letter3 == true){
			newWord= newWord+ word.charAt(3);
			
		}
		else {
			newWord = newWord+" _ ";
		}
		System.out.println("Your result is "+ newWord);
	}
	public static  void runGame(String word){
		boolean letter0 = false;
		boolean letter1= false;
		boolean letter2 = false;
		boolean letter3 = false;
		int numberOfGuess = 0;
	
		
		while(numberOfGuess < 6 && !(letter0 && letter1 && letter2 && letter3)){

			Scanner reader = new Scanner (System.in);
			
			System.out.println("Guess a letter");
			char guess = reader.nextLine().charAt(0);
			guess =toUpperCase(guess);
			
			if(isLetterInWord(word, guess)==0){
				letter0=true;
			}
			if(isLetterInWord(word, guess)==1){
				letter1=true;
			}
			if(isLetterInWord(word, guess)==2){
				letter2=true;
			}
			if(isLetterInWord(word, guess)==3){
				letter3=true;
			}
            if(isLetterInWord(word, guess)==-1){
				numberOfGuess++;
			}
			
			printWork(word, letter0, letter1, letter2, letter3);
		}
		if (numberOfGuess==6){
		System.out.println("Oops! Better luck next time :)" );
		}
		if(letter0 && letter1 && letter2 && letter3){
		System.out.println("Congrats! You got it :)");
		}
	}
}