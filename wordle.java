import java.util.Scanner;
import java.util.Random;
public class wordle{
	public static void main(String[] args){
		runGame();
	}
	public static String generateWord(){
		String[] words = {"zesty","quack","snort","funky","money","crazy","zibra","zonky","shrek",
		"feral","rogue","saucy","whale","olive","mango","fruit","icons","fairy","drown","crime"};
		Random rando = new Random();
		int w = rando.nextInt(words.length); //return the position of the randomly chosen word
		
		String wordToGuess = words[w]; //find me the random word at that position
		wordToGuess = wordToGuess.toUpperCase();
		return wordToGuess; // now give it to me
	}
	public static boolean letterInWord(String word,char letter){  //is the letter guessed in the word chosen ,true or false
		boolean truth = false;
		for (int i= 0; i < 5; i++){   	                         //yes or no =>is the letter there?
			if (word.charAt(i)== letter){
				truth = true;
			}
		}
		return truth;
	}
	public static boolean letterInSlot(String word, char letter, int position){ //is the letter guessed at the same spot as the one
		if(word.charAt(position) == letter){                                   //in the chosen word, true or fase
			return true;                                                       //yes or no => is the letter in the right position?
		}
		else{
			return false;
		}
	}
	public static String[] guessWord(String answer,String guess){ //if it is make it green, if it's not yellow and
							// 0        1       2                   if it's not in the word at all make it white
		String[] colours = {"green","yellow","white"};            // this function => know where you are at in the game 
		String[] newColours = new String [5];                                  //but doesn't print the colors yet for the user to see
		
		for(int i= 0; i < 5; i++){   	                         
			char letter = guess.charAt(i);
			
			if(letterInWord(answer,letter) == false) {
				newColours[i] = colours[2];
			}
			if(letterInWord(answer,letter) == true) {
				newColours[i] = colours[1];
			}
			if(letterInSlot(answer,letter,i) == true) {
				newColours[i] = colours[0];
			}
		}
		return newColours;	
	}
	public static String presentResults(String word,String[] colours){ //this allows the user to actually see the colours now
		final String GREENPRINT ="\u001B[32m";
		final String YELLOWPRINT = "\u001B[33m";
		final String WHITEPRINT ="\u001B[0m";
		String look = "";
		for(int i= 0; i < 5; i++){   	                         
			char letter = word.charAt(i);
			if (colours[i].equals("green")){
				look+= GREENPRINT + letter + WHITEPRINT;
			}
			if(colours[i].equals("yellow")){
				look+= YELLOWPRINT + letter + WHITEPRINT;
			}
			if(colours[i].equals("white")){
				look+= WHITEPRINT + letter;
			}
		}
		return look;
	}
	public static String readGuess() { //this asks the user to make a guess
		Scanner reader = new Scanner(System.in);
		System.out.print("Guess a word: ");
		String guess = reader.nextLine(); //user is giving me his guess
		guess = guess.toUpperCase(); //this is returning that guess toUpperCase
		
		if(guess.length() != 5) {
			return "Please write a valid guess";
		}
		return guess;
	}
	public static void runGame() { 
		int misses = 0;
		String guess = readGuess();
		String answer = generateWord();
		
		while(misses < 6 && !(guess.equals(answer))){
			if(guess.length() != 5){
				System.out.println(guess);
				guess = readGuess();
			}
			else{
				System.out.println(presentResults(guess,guessWord(answer,guess)));
				guess = readGuess();
				misses++;
			}
		}
		if(guess.equals(answer)){
			System.out.println("Good job budd! You win");
		}
		else{
			System.out.println("Wrong! Try again");
		}
	}
}
